'use strict'

const Server = require('socket.io');
const _ = require('lodash');
const Rx = require('rx');
const uuid = require('uuid');

const logger = require('./logger');
const config = require('../../config');

const messageTypes = require('../protocol/message-types');

const gameLogic = require('../game/logic');
const gameConstants = require('../game/constants');
const gameHelpers = require('../game/helpers');

const io = Server(config.get(config.KEYS.PORT), {
    serveClient : false
});

logger.info(`Server started on localhost:${config.get(config.KEYS.PORT)}`)

const UUID_KEY = Symbol();
let loggedIn = {};

const maxPlayers = config.get(config.KEYS.MAX_PLAYERS);
const moveTimeout = config.get(config.KEYS.MOVE_TIMEOUT_MS);

let game = null;

process.on('uncaughtException', (err) => {
    console.log(`Caught exception: ${err}`);
});

io.on('connection', (socket)=>{

    logger.info('new connection', socket.id);

    socket.on(messageTypes.LOGIN, onLogin);
    socket.on(messageTypes.NEW, onNew);

    socket.on(messageTypes.MOVE, (move)=>{
        logger.info('move', move);
    });

    socket.on('disconnect', onDisconnect);
    socket.on('error', (x)=>{
        logger.info('socket-error', x);
    });

    function onLogin(msg, callback){
        const count =  Object.keys(loggedIn).length;
        logger.info('login %s %s -- count: %s', msg.uuid, msg.name, count);
        if(count < maxPlayers){

            if(loggedIn[msg.uuid]){
                callback({
                    type : messageTypes.REJECT,
                    payload: 'You are already logged-in.',
                });
            }
            else {
                socket[UUID_KEY] = msg.uuid;
                loggedIn[msg.uuid] = {
                    uuid : msg.uuid,
                    name : msg.name,
                    socket : socket
                }
                callback({type:messageTypes.OK});
                broadcastPlayers(msg.name + ' joined.');
                socket.emit(messageTypes.GAME, {game:getClientVersionOfGame()});
            }

        }
        else {
            callback({
                type : messageTypes.REJECT,
                payload: 'The table if full.',
            });
        }
    }

    function onNew(msg, callback){

        logger.info('new game - request');

        // very unlikely and ignorable anyway
        if(game && !game.isOver){
          //  callback({type:messageTypes.REJECT})
        }
        else {
            game = gameLogic.createGame({
                players : _.values(loggedIn).map(p => ({name:p.name, uuid:p.uuid}))
            });
            game.id = uuid.v4();

            broadcastGame('game initialized');

            handleMoves()
                .subscribeOnNext(()=>console.log('move next'));

        }

    }

    function onDisconnect(){
        if(socket[UUID_KEY]){
            const info = loggedIn[socket[UUID_KEY]];
            delete loggedIn[socket[UUID_KEY]];
            logger.info('disconnect %s %s', info.uuid, info.name);
            broadcastPlayers(info.name + ' left.');
        }
    }

});

// +------------------+
// | AUXILIARY        |
// +------------------+

function handleMoves(){

    if(game.isOver){

        broadcastGame('game is over');
        return Rx.Observable.empty();

    }
    else {
        const player = game.currentPlayer;
        // handle situation when socket not found
        const socket = loggedIn[player.uuid] && loggedIn[player.uuid].socket;

        let playerMove = null;
        if(!socket){
            playerMove = Rx.Observable.just({
                move : gameConstants.MOVES.STICK,
                updateMsg : 'Player not present: stick applied.'
            });
        }
        else {
            playerMove = Rx.Observable.fromEvent(loggedIn[player.uuid].socket, messageTypes.MOVE)
                .map(m => ({
                    move : m,
                    updateMsg : 'Player decided to: ' + m + '.'
                }));

            const timeoutMove = Rx.Observable.timer(moveTimeout).map(_ => ({
                move : gameConstants.MOVES.STICK,
                updateMsg : 'Player did not respond: stick applied.'
            }));

            playerMove = playerMove.merge(timeoutMove);

        };

        let start = Date.now();
        Rx.Observable
            .interval(1000)
            .takeUntil(playerMove)
            .startWith(true)
            .subscribe(
                ()=> io.emit(messageTypes.TIME_LEFT, Math.round((moveTimeout + start - Date.now())/1000)),
                _ => _,
                ()=> io.emit(messageTypes.TIME_LEFT, null));

        return playerMove
            .take(1)
            .tap(x => logger.info('move', x))
            .tap(applyMove)
            .flatMap(handleMoves);

    }

}

function applyMove(move){
    console.log('applying move',move);
    game.makeMove(move.move);

    broadcastGame(move.updateMsg);
}

function getClientVersionOfGame(){

    if(game){
        const hands = _.cloneDeep(game.state.hands);
        const isOver = game.isOver;
        hands.forEach(h => {
            if(h.player.role === gameConstants.ROLES.DEALER){
                // hide dealer first card
                delete h.result;
               if(!isOver) {
                   h.value = '?';
                   h.cards[0].rank = 0;
                   h.cards[0].suit = '?';
               }
            }
            else {
                h.player = h.player.original;
            }
            delete h.next; // remove circular reference
            h.cards = h.cards.map(c => gameHelpers.getCardRepresentation(c));
        });
        return {
            id: game.id,
            isOver : isOver,
            possibleMoves : game.isOver ? [] : game.possibleMoves,
            hands
        };
    }
    else {
        return null;
    }

}

function broadcastGame(msg){
    io.emit(messageTypes.GAME, {
        game : getClientVersionOfGame(),
        updateMsg : msg,
    })
}

function broadcastPlayers(msg){
    io.emit(messageTypes.PLAYERS, {
        players : _.values(loggedIn).map(v => v.name),
        updateMsg : msg,
    })
}