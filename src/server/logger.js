'use strict'

const winston = require('winston');
const config = require('../../config');

const transports= [
    new (winston.transports.File)({
        filename: config.get(config.KEYS.LOG_PATH),
        maxsize : config.get(config.KEYS.LOG_MAX_SIZE)
    })
];

let level = 'warn';

if(config.isDev()){
    level = 'debug';
    transports.push(new (winston.transports.Console)())
}

const logger = new (winston.Logger)({
    exitOnError : false,
    level : level,
    transports : transports
});


module.exports = logger;