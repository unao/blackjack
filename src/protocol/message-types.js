module.exports = {

    // -> : SERVER SENDS TO CLIENT
    // <- : CLIENT SENDS TO SERVER
    //    : BOTH DIRECTIONS

    OK : 'ok',            // -> : accepts request
    REJECT : 'rej',       // -> : reject request

    LOGIN : 'login',      // <- : {uuid, name}

    PLAYERS : 'players',  // -> : {players, updateMsg}

    NEW : 'new',          // <- : initializing new game -- server may reject

    GAME : 'game',        // -> : {state,move,updateMsg}|null -- server sends all state needed to render; null denotes no game
    MOVE : 'move',        // <- : {move} -- clients move
    TIME_LEFT : 'time',   // -> : {timeLeft} -- how much time there is still to make the move

    LEAVE : 'leave',      //  : client says 'good-bye' - server requests client to leave

}