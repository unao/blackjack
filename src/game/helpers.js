'use strict'

const _ = require('lodash');

const constants = require('./constants');

module.exports = {
    verifyParams,

    generateOrderedDeck,
    generateShuffledDeck,
    evaluateCards, // [cards] -> [int] -- all possible evaluations
    getBestEvaluation, // [cards] -> int -- negative if busted

    getCardRepresentation,

    hitTo17Strategy,
}

function verifyParams(params){

    // this function could be written in a much more general way
    // there is a lot of type checking here due to dynamic nature of JS
    // ideally all interfaces were formally specified and automatically checked

    if (!params || typeof params !== 'object') {
        throw new Error('Missing params.');
    }

    if (!Array.isArray(params.players) ||
        params.players.length === 0 || params.players.length > params.config.maxPlayers) {
        throw new Error(`Please pass players array with 1 - ${params.config.maxPlayers} items.`);
    }

}

function generateOrderedDeck(){

    return _.values(constants.SUITS)
        .reduce( (acc, suit) =>
            acc.concat(generateSuit(suit)
            ), []);

    function generateSuit(suit){

        return _.range(1, constants.SUIT_LENGTH + 1).map((rank)=>({
            suit,
            rank
        }));

    }

};

function generateShuffledDeck(){
    return _.shuffle(generateOrderedDeck());
}

function evaluateCards(cards){

    return cards.reduce((acc, card)=>{

        if(card.rank === constants.ACE_RANK){
            return aceEval(acc)
        }
        else {
            return acc.map(v=> v + Math.min(card.rank, constants.MAX_NON_ACE_VALUE));
        }

    }, [0]);

    function aceEval(currentEval){

        return constants.ACE_VALUES.reduce((acc , value)=>
            acc.concat(currentEval.map(v => v + value)), []);

    }

};

function getBestEvaluation(cards){

    const evaluations = evaluateCards(cards);
    const ok = evaluations.filter(e => e <= constants.BLACKJACK_VALUE);
    return Math.max.apply(null, ok);

}

// +------------------+
// | STRATEGIES       |
// +------------------+

function hitTo17Strategy(decks){

    if(getBestEvaluation(decks.current) < 17){
        return constants.MOVES.HIT;
    }
    else {
        return constants.MOVES.STICK;
    }

};

// +------------------+
// | REPRESENTATION   |
// +------------------+

function getCardRepresentation(card){

    const color = _.includes([constants.SUITS.DIAMONDS, constants.SUITS.HEARTS], card.suit) ?
        'red' : 'black';
    let rank = null;
    switch(card.rank){
        case 0:
            rank = '?';
            break;
        case 1:
            rank = 'A'
            break;
        case 11:
            rank = 'J';
            break;
        case 12:
            rank = 'Q';
            break;
        case 13:
            rank = 'K';
            break;
        default:
            rank = card.rank;
    }
    let suit = '?';
    switch(card.suit){
        case constants.SUITS.SPADES:
            suit = '\u2660';
            break;
        case constants.SUITS.HEARTS:
            suit = '\u2665';
            break;
        case constants.SUITS.DIAMONDS:
            suit = '\u2666';
            break;
        case constants.SUITS.CLUBS:
            suit = '\u2663';
            break;
    }

    return {
        color,
        rank,
        suit
    }

}