'use strict'

module.exports = {

    maxPlayers: 7, // limit of concurrent players
    deck: null, // custom deck may be passed (helpful especially in testing)
    dealerStrategy: null, // custom strategy may be used: decks -> MOVES.STICK|MOVES.HIT -- decks = {current, all}

};