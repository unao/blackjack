'use strict'

const _  = require('lodash');

const constants = require('./constants');
const config = require('./config');
const helpers = require('./helpers');

// even if one thing is being exported right now
// it is better to keep it easily extendable
module.exports = {
    createGame
};

// +------------------+
// | PUBLIC           |
// +------------------+

function createGame(params){

    // defaults config
    params.config = _.assign({}, config, params.config);
    if(!params.config.dealerStrategy){
        params.config.dealerStrategy = helpers.hitTo17Strategy;
    }

    helpers.verifyParams(params);

    let state = initGame(params);

    const game = {
        makeMove : (move) => {

            const moveInfo = makeMove(game, move); // {state, dealerMoves, isOver}
            state = moveInfo.state;
            moveInfo.dealersMove = handleDealer(game);

            if(game.isOver){
                evaluateResults(game);
            }

            return moveInfo;
        },
        get config(){
            return params.config;
        },
        get state() {
            return state;
        },
        get isOver(){
            return isOver(game);
        },
        get currentPlayer(){
            return throwIfOver(()=>{
                return game.currentHand.player.original;
            });
        },
        get currentHand(){
            return throwIfOver(()=>{
                return state.hands.filter(h => h.current)[0];
            });
        },
        get possibleMoves(){
            return throwIfOver(()=>{
                const ms = [constants.MOVES.HIT, constants.MOVES.STICK];
                if(game.currentHand.cards.length == 2 &&
                    game.currentHand.cards[0].rank === game.currentHand.cards[1].rank &&
                    game.currentHand.player.role === constants.ROLES.PLAYER &&
                        // allow to split only once
                    state.hands.filter(h => h.player === game.currentHand.player).length === 1){
                    ms.push(constants.MOVES.SPLIT)
                }
                return ms;
            });
        },
        get getResults(){
            return game.state.hands
                .filter(h => h.player.role !== constants.ROLES.DEALER)
                .map((h)=>{
                    return [h.player.original, h.result, h.cards];
                });
        }
    };

    return game;

    function throwIfOver(fn){

        if(game.isOver){
            throw new Error('Game is over.');
        }
        else {
            return fn();
        }

    }

}


// +------------------+
// | PRIVATE          |
// +------------------+

function initGame(params){

    const players = initPlayers(params.players);
    const deck = _.cloneDeep(params.config.deck) || helpers.generateShuffledDeck();
    const hands = players.map(p => ({
        player : p,
        cards: deck.splice(0,constants.HAND_START_LENGTH),
        active: true,
    }));
    hands.forEach(h => h.value = helpers.getBestEvaluation(h.cards));

    updateHandsOrder(hands);
    hands[0].current = true;

    return {
        players,
        deck,
        hands,
    };

    function initPlayers(players){

        players = players.map((p,i) => ({
            original : p,
            name : p.name || 'player_' + i,
            role : constants.ROLES.PLAYER
        }));
        players.push({
            role : constants.ROLES.DEALER,
            name : constants.ROLES.DEALER,
        });
        return players;

    }

};

function updateHandsOrder(hands){

    // create a circular graph of hands that are still active
    hands
        .filter(h => h.active)
        .forEach((h,i, hs)=>h.next = hs[(i+1)%hs.length])

};

function isOver(game){
    // either all done or dealer busted
    return game.state.hands.filter(h => h.active).length === 0 ||
        didDealerBust(game.state.hands) || game.state.deck.length === 0;
}

function didDealerBust(hands){

    return getDealerResult(hands) < 0;

};

function getDealerResult(hands){

    return hands.filter(h => h.player.role === constants.ROLES.DEALER)
        .map(h => h.cards)
        .map(helpers.getBestEvaluation)
        .reduce((acc, curr)=>acc+curr, 0)

};

function makeMove(game, move){

    if(!_.includes(game.possibleMoves, move)){
        throw new Error('Illegal move.');
    }

    const currentHand = game.currentHand;
    let hitCard;

    switch(move){

        case constants.MOVES.HIT:
            hitCard = game.state.deck.shift();
            currentHand.cards.push(hitCard);
            const value = helpers.getBestEvaluation(currentHand.cards);
            currentHand.value = value;
            if (value < 0){
                currentHand.active = false;
                currentHand.result = constants.RESULTS.LOST_BUSTED;
            }
            else if (value === constants.BLACKJACK_VALUE){
                currentHand.active = false;
            }
            break;
        case constants.MOVES.STICK:
            currentHand.active = false;
            break;
        case constants.MOVES.SPLIT:
            // value/2 is wrong in case of aces
            let newHand = _.cloneDeep(currentHand);
            newHand.player.original = currentHand.player.original;
            delete newHand.current;
            currentHand.cards.pop();
            newHand.cards.shift();
            newHand.value = currentHand.value = helpers.getBestEvaluation(currentHand.cards);
            game.state.hands.splice(_.indexOf(game.state.hands, currentHand), 0, newHand);
            break;
        default:
            throw new Error('Unknown move.')

    }

    delete currentHand.current;
    currentHand.next.current = true;
    updateHandsOrder(game.state.hands);

    return {
        state : game.state,
        move : move,
        card : hitCard,
    }

};

function handleDealer(game){

    const acc = [];

    if(game.state.hands.filter(h => h.result !== constants.RESULTS.LOST_BUSTED).length
        === 0){
        return acc;
    }

    while(!game.isOver &&
    game.currentHand.player.role === constants.ROLES.DEALER
    && game.currentHand.active){
        const move = game.config.dealerStrategy({
            current : game.currentHand.cards,
            all : game.state.hands.map(h => h.cards),
        });
        acc.push(makeMove(game, move));
    };
    return acc;
}

function evaluateResults(game){

    const dealerValue = getDealerResult(game.state.hands);

    game.state.hands.forEach(h => {

        const value = helpers.getBestEvaluation(h.cards);

        if(h.player.role === constants.ROLES.DEALER){
            return;
        }

        h.value = value;
        h.active = false;
        delete h.current;

        if(value < 0){
            h.result = constants.RESULTS.LOST_BUSTED;
        }
        else if (dealerValue < 0) {
            h.result = constants.RESULTS.WON;
        }
        else if(dealerValue > value) {
            h.result = constants.RESULTS.LOST_WEAKER;
        }
        else if(dealerValue == value){
            h.result = constants.RESULTS.DRAW;
        }
        else {
            h.result = constants.RESULTS.WON;
        }

    })

}