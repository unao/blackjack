'use strict'

jest.dontMock('../logic');
jest.dontMock('../constants');
jest.dontMock('../config');
jest.dontMock('../helpers');

const _ = require('lodash');

const logic = require('../logic');
const helpers = require('../helpers');
const config = require('../config');
const constants = require('../constants');

describe('single player game', ()=>{

    let players, game;

    const ranks = [
        10, 5, // player initial hand
        1, 1, // dealer initial hand
        4, 3, 1, 8, 9, 10, 7, 2, 1, 8, 4, 11, 4, 12];
    const pseudoDeck = ranks.map(rank => ({rank}));

    beforeEach(()=>{
        players = [{name:'p1'}];
        game = logic.createGame({
            players,
            config: {
                deck : pseudoDeck,
           //     dealerStrategy : helpers.hitTo17Strategy,
            }
        });
    });

    it('player should have 15 points at the beginning', ()=>{

        expect(helpers.evaluateCards(game.currentHand.cards)[0]).toBe(15);

    });

    it('player should be able to hit or stick', ()=>{

        expect(game.possibleMoves.length).toBe(2);
        expect(_.includes(game.possibleMoves, constants.MOVES.HIT)).toBe(true);
        expect(_.includes(game.possibleMoves, constants.MOVES.STICK)).toBe(true);

    });

    it('after stick the game should be over and player should lose', ()=>{

        game.makeMove(constants.MOVES.STICK);
        expect(game.isOver).toBe(true);
        expect(game.getResults[0][1]).toBe(constants.RESULTS.LOST_WEAKER);

    });

    it('after hit once player should win', ()=>{

        game.makeMove(constants.MOVES.HIT);
        game.makeMove(constants.MOVES.STICK);
        expect(game.isOver).toBe(true);
        expect(game.getResults[0][1]).toBe(constants.RESULTS.WON);

    });

    it('after hit twice player should win', ()=>{

        game.makeMove(constants.MOVES.HIT);
        game.makeMove(constants.MOVES.HIT);
        game.makeMove(constants.MOVES.STICK);
        expect(game.isOver).toBe(true);
        expect(game.getResults[0][1]).toBe(constants.RESULTS.WON);

    });

    it('after hit three times player should bust and lose', ()=>{

        game.makeMove(constants.MOVES.HIT);
        game.makeMove(constants.MOVES.HIT);
        game.makeMove(constants.MOVES.HIT);
        expect(game.isOver).toBe(true);
        expect(game.getResults[0][1]).toBe(constants.RESULTS.LOST_BUSTED);

    });

});

describe('multiple players - hitTo17Strategy - sorted cards', ()=>{

    let players, game;

    beforeEach(()=>{
        players = [{name:'p1'},{name:'p2'},{name:'p3'}];
        game = logic.createGame({
            players,
            config: {
                deck : helpers.generateOrderedDeck().sort((a,b)=>b.rank - a.rank),
            }
        });
    });

    it('all players should lose', ()=>{

        while(!game.isOver){
            game.makeMove(constants.MOVES.HIT);
        }
        expect(game.getResults[0][1]).toBe(constants.RESULTS.LOST_BUSTED);
        expect(game.getResults[1][1]).toBe(constants.RESULTS.LOST_BUSTED);
        expect(game.getResults[2][1]).toBe(constants.RESULTS.LOST_BUSTED);

    });

    it('all players should draw', ()=>{

        while(!game.isOver){
            game.makeMove(constants.MOVES.STICK);
        }
        expect(game.getResults[0][1]).toBe(constants.RESULTS.DRAW);
        expect(game.getResults[1][1]).toBe(constants.RESULTS.DRAW);
        expect(game.getResults[2][1]).toBe(constants.RESULTS.DRAW);

    });

});

describe('multiple players - dealer always hits - sorted cards', ()=>{

    let players, game;

    beforeEach(()=>{
        players = [{name:'p1'},{name:'p2'},{name:'p3'}];
        game = logic.createGame({
            players,
            config: {
                deck : helpers.generateOrderedDeck().sort((a,b)=>b.rank - a.rank),
                 dealerStrategy : _ => constants.MOVES.HIT,
            }
        });
    });

    it('all players should lose', ()=>{

        while(!game.isOver){
            game.makeMove(constants.MOVES.HIT);
        }
        expect(game.getResults[0][1]).toBe(constants.RESULTS.LOST_BUSTED);
        expect(game.getResults[1][1]).toBe(constants.RESULTS.LOST_BUSTED);
        expect(game.getResults[2][1]).toBe(constants.RESULTS.LOST_BUSTED);

    });

    it('all players should win', ()=>{

        while(!game.isOver){
            game.makeMove(constants.MOVES.STICK);
        }
        expect(game.getResults[0][1]).toBe(constants.RESULTS.WON);
        expect(game.getResults[1][1]).toBe(constants.RESULTS.WON);
        expect(game.getResults[2][1]).toBe(constants.RESULTS.WON);

    });

});

describe('splitting', ()=>{

    let players, game;

    beforeEach(()=>{
        players = [{name:'p1'},{name:'p2'}];
        game = logic.createGame({
            players,
            config: {
                deck : helpers.generateOrderedDeck().sort((a,b)=>a.rank - b.rank),
                dealerStrategy : _ => constants.MOVES.HIT,
            }
        });
    });

    it('split should be among possible moves ', ()=>{

        expect(_.includes(game.possibleMoves, constants.MOVES.SPLIT)).toBe(true);
        game.makeMove(constants.MOVES.STICK);
        expect(_.includes(game.possibleMoves, constants.MOVES.SPLIT)).toBe(true);

    });

    it('players should be able to use split', ()=>{

        game.makeMove(constants.MOVES.SPLIT);
        expect(game.state.hands.length).toBe(players.length + 1 + 1); // split, dealer
        expect(game.state.hands[0].player.original === game.state.hands[1].player.original).toBe(true);
        expect(game.currentPlayer).toBe(players[1]);

    })

})