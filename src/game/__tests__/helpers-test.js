'use strict'

jest.dontMock('../constants');
jest.dontMock('../helpers');

const _ = require('lodash');

const constants = require('../constants');
const helpers = require('../helpers');

describe('generating deck', ()=>{

    // shuffled by lodash so no need to test it here

    it(`deck should have ${constants.DECK_LENGTH} cards`, ()=>{

        const deck = helpers.generateShuffledDeck();
        expect(deck.length).toBe(constants.DECK_LENGTH);

    })

});

describe('evaluating cards', ()=>{

    it('should sum up ranks', ()=>{

        const ranks = _.range(2,11);
        const sum = _.sum(ranks);

        // suits not needed
        const pseudoCards = ranks.map(r => ({rank : r}));
        const evaluations = helpers.evaluateCards(pseudoCards);

        expect(evaluations.length).toBe(1);
        expect(evaluations[0]).toBe(sum);

    });

    it('should handle aces properly', ()=>{

        const pseudoCards = _.range(4).map(()=>({rank: constants.ACE_RANK}));
        const evaluations = helpers.evaluateCards(pseudoCards);

        expect(evaluations.length).toBe(16); // 2^4
        expect(Math.min.apply(null, evaluations)).toBe(4*Math.min.apply(null, constants.ACE_VALUES));
        expect(Math.max.apply(null, evaluations)).toBe(4*Math.max.apply(null, constants.ACE_VALUES));

    });

    it('should evaluate properly a mixed hand', ()=>{

        const pseudoCards = [{rank:1},{rank:5},{rank:11}, {rank:12}];
        const evaluations = helpers.evaluateCards(pseudoCards);

        expect(evaluations.length).toBe(2);
        expect(evaluations.indexOf(1+5+10+10)).toBeGreaterThan(-1);
        expect(evaluations.indexOf(11+5+10+10)).toBeGreaterThan(-1);

    })

});

describe('calculating best evaluation', ()=>{

    it('should return 20', ()=>{

        const cards = [1, 9, 10].map(rank => ({rank}));
        expect(helpers.getBestEvaluation(cards)).toEqual(20);

    });

    it('should return 14', ()=>{

        const cards = [1, 1, 2, 10].map(rank => ({rank}));;
        expect(helpers.getBestEvaluation(cards)).toEqual(14);

    });

    it('should return negative number', ()=>{

        const cards = [5, 5, 6, 7].map(rank => ({rank}));;
        expect(helpers.getBestEvaluation(cards)).toBeLessThan(0);

    });

});


describe('hitTo17Strategy', ()=>{

    const hitDeck = {
        current : [5, 5, 6].map(rank => ({rank}))
    };

   const stickDeck = {
        current : [10, 7].map(rank => ({rank}))
   };

    it('should hit', ()=>{
        expect(helpers.hitTo17Strategy(hitDeck)).toBe(constants.MOVES.HIT);
    })

    it('should stick', ()=>{
        expect(helpers.hitTo17Strategy(stickDeck)).toBe(constants.MOVES.STICK);
    })

});