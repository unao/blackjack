'use strict'

jest.dontMock('../logic');
jest.dontMock('../constants');
jest.dontMock('../config');
jest.dontMock('../helpers');

const _ = require('lodash');

const logic = require('../logic');
const config = require('../config');
const constants = require('../constants');

describe('check params', ()=>{

    it('wrong params should throw', ()=>{

        expect(logic.createGame).toThrow();
        expect(logic.createGame.bind(null, {})).toThrow();
        expect(logic.createGame.bind(null, {players : null})).toThrow();


    });

    it('incorrect players number should throw', ()=>{

        expect(logic.createGame.bind(null, {players: []})).toThrow();
        expect(logic.createGame.bind(null, {players: _.range(config.maxPlayers + 1)})).toThrow();

    });

    it('should be possible to set custom number of players', ()=>{

        const maxPlayers = 2;

        const params = {
            players: _.range(maxPlayers+1),
            config: {
                maxPlayers : maxPlayers,
            },
        };

        expect(logic.createGame.bind(null, params)).toThrow();

    });

    it('correct params should pass', ()=>{

        const maxPlayers = _.random(1, 5);
        const params = {
            players: _.range(maxPlayers),
            config: {
                maxPlayers : maxPlayers,
            },
        };

        expect(logic.createGame.bind(null, params)).toBeTruthy();

    });

});

describe('initial state', ()=>{

    const players = [{name: 'p1'}, {name: 'p2'}, {name: 'p3'}];
    const game = logic.createGame({
        players,
    });
    const state = game.state;
    const cardsUsed = (players.length + 1) * constants.HAND_START_LENGTH; // 1 for dealer

    it('number of players and hands should equal', ()=>{

        expect(state.hands).toBeTruthy();
        expect(state.players.length).toEqual(state.hands.length);

    });

    it(`each hand should have exactly ${constants.HAND_START_LENGTH} cards`, ()=>{

        _.range(3).forEach(i => {
            expect(state.hands[i].cards).toBeDefined();
            expect(state.hands[i].cards.length).toBe(constants.HAND_START_LENGTH);
        });

    });

    it(`should be ${cardsUsed} less cards in the deck`, ()=>{

        expect(state.deck.length).toEqual(constants.DECK_LENGTH - cardsUsed);

    });

    it('hands should form a circular graph', ()=>{

        expect(game.currentHand).toBeDefined();
        expect(game.currentHand.next).toBeDefined();
        expect(game.currentHand.next).not.toBe(state.currentHand);
        expect(state.hands[state.hands.length-1].next).toBe(game.currentHand);

    });

    it('0-th player should be set as current', ()=>{

        expect(game.currentPlayer).toBe(players[0]);

    });

    it('game should not be over when started', ()=>{

        expect(game.isOver).toBe(false);

    });

    it('game default strategy should set if no custom', ()=>{

        expect(game.config.dealerStrategy).toBeDefined();

    });

    it('should be possible to set custom strategy for dealer', ()=>{

        const game = logic.createGame({
            players : [{name: 'p1'}],
            config : {
                dealerStrategy : dealerStrategy
            }
        });
        function dealerStrategy(){};

        expect(game.config.dealerStrategy).toBe(dealerStrategy);

    });

});