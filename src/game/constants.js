'use strict'

module.exports = {

    BLACKJACK_VALUE : 21,
    SUIT_LENGTH : 13,
    DECK_LENGTH : 52,
    HAND_START_LENGTH : 2,
    ACE_RANK : 1,
    ACE_VALUES : [1,11],
    MAX_NON_ACE_VALUE: 10,

    ROLES : {
        DEALER : 'dealer',
        PLAYER : 'player',
    },

    SUITS : {
        SPADES : 'spades',
        HEARTS : 'hearts',
        DIAMONDS : 'diams',
        CLUBS : 'clubs'
    },

    MOVES : {
        HIT : 'hit',
        STICK : 'stick',
        SPLIT : 'split',
    },

    // from the player perspective
    RESULTS : {
        WON : 'won',
        DRAW : 'draw',
        LOST_WEAKER : 'lost weaker',
        LOST_BUSTED : 'lost bust',
    },

}