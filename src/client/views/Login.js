import React from 'react';

import Dialog from 'material-ui/lib/dialog';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';


export default (props) => {

    if(!props || !props.updateName || !props.login){
        return  false;
    }

    // this check should be moved to some configuration file
    const canLogin = props.name && props.name.length >= 3;

    const actions = [
        <RaisedButton
            label="Login"
            primary={true}
            keyboardFocused={true}
            disabled={!canLogin}
            onMouseDown={()=>props.login(true)}
        />,
    ];

    return  (<Dialog
        title="Please specify your name"
        actions={actions}
        modal={true}
        open={true}
    >
        <TextField defaultValue={props.name}
                   onChange={(e)=>props.updateName(e.target.value)} />
    </Dialog>)

}