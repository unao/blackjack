import React from 'react';
import Paper from 'material-ui/lib/paper';
import Divider from 'material-ui/lib/divider';
import FlatButton from 'material-ui/lib/flat-button';
import RaisedButton from 'material-ui/lib/raised-button';
import Badge from 'material-ui/lib/badge';

import constants from '../../game/constants';

export default (props) => {

    const {
        player,
        cards,
        me,
        active,
        current,
        possibleMoves,
        makeMove,
        result,
        value,
        timeLeft,
        } = props;

    const zDepth = (current && active) || result === constants.RESULTS.WON ? 5 : 1;
    const meText = me ? '(you) ' : '';

    let moveBtns = null;
    if(current && me){
        moveBtns = (<div style={{padding: 20}}>
            {possibleMoves.map(m => moveToButton(m, makeMove))}
        </div>)
    }

    return (
        <Paper className={'hand'}
               zDepth={zDepth}>

            <div  className={(player.role === constants.ROLES.DEALER ? 'dealer ' : '')}>
                <small>{meText}</small>
                <span>{player.name}</span>
                <span className={ (timeLeft <= 5) ? 'redPulse' : ''} style={{float:'right'}}>{timeLeft}</span>
            </div>
            {moveBtns}
            <div style={{padding:20}}>
                <Badge
                    badgeContent={value || 'x'}
                    primary={true}
                >
                    {cards.map(c => {
                        return <span className={'card ' + c.color} key={c.rank + c.suit}>{c.rank+c.suit+' '}</span>
                    })}
                </Badge>
            </div>
            <div>
                <h4 className={resultToColor(result)}>{result && result.toUpperCase()}</h4>
            </div>

        </Paper>)

}

function resultToColor(result){

    if(!result || result === constants.RESULTS.DRAW){
        return 'black';
    }
    else if(result === constants.RESULTS.WON){
        return 'green';
    }
    else {
        return 'red';
    }

}

function moveToButton(move, makeMove){

    let cb = ()=>makeMove(move);
    if(move === constants.MOVES.HIT){
        return <RaisedButton label="hit" primary={true} onMouseDown={cb} />
    }
    else if(move === constants.MOVES.STICK) {
        return <FlatButton label="stick" onMouseDown={cb} />
    }
    else if(move === constants.MOVES.SPLIT) {
        return <RaisedButton label="split" secondary={true} onMouseDown={cb} />
    }
    else {
        throw new Error('Unknown move: ' + move);
    }

}