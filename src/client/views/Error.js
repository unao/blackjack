import React from 'react';

import Dialog from 'material-ui/lib/dialog';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';

export default (props) => {

    console.log(props);

    const actions = [
        <RaisedButton
            label="reload"
            primary={true}
            keyboardFocused={true}
            onMouseDown={props.action}
        />,
    ];

    return  <Dialog
        title="Whoops, an error occurred."
        actions={actions}
        modal={true}
        open={true}
    >
        <p>
            {props.message}
        </p>
    </Dialog>

}