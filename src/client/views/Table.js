import React from 'react';
import AppBar from 'material-ui/lib/app-bar';
import RaisedButton from 'material-ui/lib/raised-button';
import LinearProgress from 'material-ui/lib/linear-progress';
import Paper from 'material-ui/lib/paper';

import Hand from './Hand';

export default (props)=>{

    if(!props || !props.players){
        return <LinearProgress mode="indeterminate"/>
    }

    console.log(props.gamesHistory, 'HISTORY');

    let content = null;
    let history = null;
    if(props.game && !props.game.isOver){
        content = renderGame(props);
    }
    else if(!props.game || props.game.isOver) {

        const otherCount = (props.players && props.players.length-1) || 0;
        let otherMessage = null
        switch(otherCount){
            case 0:
                otherMessage = 'You are here alone.';
                break;
            case 1:
                otherMessage = 'There is one other player here.';
                break;
            default:
                otherMessage = `There are ${otherCount} other players here.`
        };

        content  = (
            <Paper style={{width: 350, padding:20, margin:20}}>
                <p>No game underway currently. {otherMessage}</p>
                <RaisedButton primary={true} label="New Game" onMouseDown={props.newGame} />
            </Paper>
        );

        history = (props.gamesHistory||[]).map((g,i) => {
            return (<div key={i}>
                <hr/>
                {renderGame({
                    game:g,
                    uuid : props.uuid
                })}
            </div>)
        });

    }

    return (<div>
        <AppBar
            title="Blackjack"
            showMenuIconButton={false}
        />
        {content}
        {history}
    </div>)

}

function renderGame(props){

    return props.game.hands.map((h,i) => {
        if(h.player.uuid === props.uuid){
            h.me = true;
            if(h.active && !props.game.isOver){
                h.possibleMoves = props.game.possibleMoves;
                h.makeMove = props.makeMove;
            }
        }
        h.isOver = props.game.isOver;
        if(h.current && !h.isOver){
            h.timeLeft = props.timeLeft;
        }
        return <Hand key={i} {...h}/>
    });

}