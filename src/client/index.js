import React from 'react';
import ReactDOM from 'react-dom';

import * as messageTypes from '../protocol/message-types';

import uuid from 'uuid';

import clientIO from 'socket.io-client';

import Rx from 'rx';
import {observeProps, createEventHandler} from 'rx-recompose';

import Snackbar from 'material-ui/lib/snackbar';

import Login from './views/Login.js';
import Error from './views/Error.js';
import Table from './views/Table.js';

require('./styles.css');

const ORIGIN = location.protocol + '//' + __HOST__;
const CONNECTION_TIMEOUT = __FIRST_CONNECTION_TIMEOUT__;

const UUID_STORAGE_KEY = 'uuid';
const NAME_STORAGE_KEY = 'name';

const ROOT_ELEMENT = document.getElementById('root');
const SNACKBAR_ROOT = document.getElementById('snackbars');

export const io = clientIO(ORIGIN, {
    reconnection : false
});

// +------------------+
// | STREAMS          |
// +------------------+

const connected = Rx.Observable.fromEvent(io, 'connect')
    .publish().refCount();

const disconnected = Rx.Observable.fromEvent(io, 'disconnect')
    .publish().refCount();

const isConnected = connected.map(_ => true)
    .merge(disconnected.flatMap(x => Rx.Observable.throw('Lost connection to the server.')))
    .merge(Rx.Observable.timer(CONNECTION_TIMEOUT)
        .flatMap(x => Rx.Observable.throw('Cannot establish connection to the server.'))
        .takeUntil(connected));

const players = Rx.Observable.fromEvent(io, messageTypes.PLAYERS)
    .tap(x => showSnackbar(x.updateMsg))
    .map(x => x.players)
    .publish().refCount();

const game = Rx.Observable.fromEvent(io, messageTypes.GAME)
    .tap(x => x.updateMsg && showSnackbar(x.updateMsg))
    .map(x => x.game)
    .publish().refCount();

const gamesHistory = game
    .filter(g => g && g.isOver)
    .distinct(g => g.id)
    .scan((acc,curr)=>{
        acc.unshift(curr);
        if(acc.length > 10){
            acc.pop();
        }
        return acc;
    }, []);


const timeLeft = Rx.Observable.fromEvent(io, messageTypes.TIME_LEFT)
    .tap(x => console.log('LEFT', x))
    .publish().refCount();

// +------------------+
// | RX-RECOMPOSE     |
// +------------------+

const updateName$ = createEventHandler();
const login$ = createEventHandler();

const RxLogin = observeProps(_ => {

    return {

        name : updateName$
            .tap(storeName)
            .startWith(getCredentials().name),
        updateName : Rx.Observable.just(updateName$),
        login : Rx.Observable.just(login$),

    }

}, Login);

const newGame$ = createEventHandler();

const RxTable = observeProps(_ => {

    return {
        players,
        game,
        timeLeft,
        gamesHistory,

        makeMove : Rx.Observable.just(makeMove),
        newGame : Rx.Observable.just(newGame$),
        uuid : Rx.Observable.just(getCredentials().uuid),

    }

}, Table);

// +------------------+
// | SUBSCRIPTIONS    |
// +------------------+

isConnected
    .tap(x => console.log('Connected: ', x))
    .tap(()=>{
        ReactDOM.render(<RxLogin />, ROOT_ELEMENT);
    })
    .flatMap(login())
    .tap(_ => showSnackbar('Successfully joined.'))
    .flatMapLatest(main())
    .tap(()=>{
        ReactDOM.render(<RxTable />, ROOT_ELEMENT);
    })
    .subscribeOnError((x)=>{
        ReactDOM.render(<Error message={x} action={()=>location.reload()} />, ROOT_ELEMENT);
    });

newGame$
    .subscribeOnNext(()=>io.emit(messageTypes.NEW));

function login(){

    return Rx.Observable.create(observer => {

     //   sendCred();
        login$.subscribeOnNext(sendCred);

        function sendCred(){
            const cred = getCredentials();
            console.log('Sending credentials', cred);
            io.emit(messageTypes.LOGIN, cred, (response)=>{
                console.log('Credentails response', response);
                if(response.type == messageTypes.OK){
                    observer.onNext();
                    observer.onCompleted();
                }
                else {
                    observer.onError(response.payload);
                }
            })
        }

    });

}

function main(){

    return Rx.Observable.create(observer => {
        observer.onNext();
    })

}

// +------------------+
// | AUXILIARY        |
// +------------------+

function makeMove(move){

    io.emit(messageTypes.MOVE, move);

}

function getCredentials(){

    let cred = {
        uuid : localStorage.getItem(UUID_STORAGE_KEY),
        name : localStorage.getItem(NAME_STORAGE_KEY) || '',
    }

    if(!cred.uuid){
        cred.uuid = uuid.v4();
        localStorage.setItem(UUID_STORAGE_KEY, cred.uuid);
    }

    return cred;
}

function storeName(name){

    localStorage.setItem(NAME_STORAGE_KEY, name);

}

function showSnackbar(message){

    // onRequestClose is a quick hack for keeping it stateless
    ReactDOM.render(
        <Snackbar open={true}
                  message={message}
                  autoHideDuration={3000}
        />,
        SNACKBAR_ROOT);
}