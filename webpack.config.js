var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var autoprefixer = require('autoprefixer');
var precss       = require('precss');

var config = require('./config');

var entry = [
  './src/client/index'
];

var plugins = [
  new webpack.DefinePlugin({ // this will be injected into client scripts
    __HOST__ : `"${config.get(config.KEYS.HOSTNAME)}:${config.get(config.KEYS.PORT)}"`,
    __FIRST_CONNECTION_TIMEOUT__ : config.get(config.KEYS.FIRST_CONNECTION_TIMEOUT_MS)
  }),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/client/index.html',
  })
];
var jsLoaders = ['babel'];
augmentIfDev();

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: entry,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'app.js',
    publicPath: '/' // only static traffic hits it anyway
  },
  plugins: plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: jsLoaders,
        include: path.join(__dirname, 'src', 'client')
      },
      {
        test:   /\.css$/,
        loader: "style-loader!css-loader!postcss-loader"
      }
    ]
  },
  postcss: function () {
    return [autoprefixer, precss];
  }
};

function augmentIfDev(){
  if(config.isDev()){
    entry.unshift(
        `webpack-dev-server/client?localhost:${config.get(config.KEYS.STATIC_PORT)}`,
        'webpack/hot/only-dev-server'
    );
    plugins.push(new webpack.HotModuleReplacementPlugin());
    jsLoaders.unshift('react-hot');
  }
}