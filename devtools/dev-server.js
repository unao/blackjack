var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var path = require('path')

var wpConfig = require('../webpack.config');
var config = require('../config');

var port = config.get(config.KEYS.STATIC_PORT);

var server = new WebpackDevServer(webpack(wpConfig), {
  publicPath: wpConfig.output.publicPath,
  hot: true,
  historyApiFallback: true
});

server.listen(port, 'localhost', function (err, result) {
  if (err) {
    console.log(err);
  }

  console.log(`Listening at localhost:${port}`);
});
