Intallation
===========

 1. copy `config/config-sample.js` to `config/config.js` and adjust if needed.
 2. run `npm install`
 3. run `npm start` (starts webpack static server)
 4. run `node src/server` (starts websocket server)

Descirption 
===========

Architures aims: 
 1. easy to reason about (have explicit and local dependecies, loosly coupling)
 2. easy to test (maximizing usage of pure function)
 3. lower risk of runtime errors (extracting magic strings/values to seperate modules)


### game

Public interface: `createGame(params)`:

 - `players` an array of objects
 - `config` allows specifiy custom deck, dealer strategy and maximum number of players.
 
After creation consequtive `makeMove`s should be called until `isOver`.

After each move additional info desribing this move is returned and optionally move(s) made by the dealer.

All opertions are `sync`.

No external dependecies (apart from `lodash`). Covered pretty extensively with tests.

### client

Client minimizes amount of internal state - maximizes reactivity to server-side events; having single source of truth.

All logic is based on `rxjs`, which imho is the most beautiful and powerful paradim for dealing with async events.

`rx-recompose` glues logic with views.

Usage of `material-ui`. All ui-components are pure functions `props -> <jsx/>` - any side effects should be done by callbacks passed via `props`. Super easy to test and reason about.

### server

Nothing fancy here - was thinking of adding `redis`, but decided that it would be an over-kill.

Two nice things:
 1. recursive usage of `handleMoves` via `.flatMap(handleMoves)`
 2. limiting response time

Most of network failures / disconnects are handled properly. 