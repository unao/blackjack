module.exports = {

    NODE_ENV : 'dev',

    HOSTNAME: 'localhost',
    PORT: '8080',

    STATIC_HOSTNAME: 'localhost',
    STATIC_PORT: '4444',

    LOG_PATH : __dirname + '/../logs/server.log',
    LOG_MAX_SIZE : 1024*1024*50,

    FIRST_CONNECTION_TIMEOUT_MS : 3*1000,

    MAX_PLAYERS : 6,
    MOVE_TIMEOUT_MS : 10*1000,

};
