'use strict'

var nconf = require('nconf');
var path = require('path');

// reconstruct configs that should be set
var keys = require('./config-sample');
Object.keys(keys).forEach(k => keys[k] = k);

let ALLOWED_ENUMS = { };
ALLOWED_ENUMS[keys.NODE_ENV] = ['prod', 'dev'];

const config = nconf
    .argv()
    .env(Object.keys(keys).map(k => keys[k]))
    .defaults(init(require('./config.js'), keys));

var errors = verify(config, keys);
if(errors.length){
    console.error(errors.join('\n'));
    throw new Error('Incorrect configuration');
}

config.KEYS = keys;
config.isDev = () => config.get(config.KEYS.NODE_ENV) === 'dev';

module.exports = config;

// +------------------+
// | PRIVATE          |
// +------------------+
function init(config, keys){

    config[keys.LOG_PATH] = path.normalize(config[keys.LOG_PATH]);

    return config;

}

function verify(config, keys){

    const realKeys = Object.keys(keys).map(k => keys[k]);

    let missing = verifyEnvs(config, realKeys).map(err => `${err} is missing.`);
    let wrong = verifyEnums(config, realKeys)
        .map(err =>
            `${err.key} has incorrect value "${err.value}".
             Please set it to one of: ${JSON.stringify(err.allowed)}.`);

    return missing.concat(wrong);

}

function verifyEnvs(config, keys){

    return keys.reduce((acc, k)=>{

        if((config.get(k) === undefined)){
            acc.push(k);
        }
        return acc;

    }, [])

}

function verifyEnums(config){

   return Object.keys(ALLOWED_ENUMS)
       .reduce((acc,k)=>{

           let value = config.get(k);
           if(value !== undefined){
               if(ALLOWED_ENUMS[k].indexOf(value) === -1){
                   acc.push({
                       key: k,
                       value : value,
                       allowed : ALLOWED_ENUMS[k],
                   })
               }
           }

           return acc;

       }, [])

}